import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';
import Transactions from '@/components/Transactions';
import Accounts from '@/components/Accounts';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/transactions',
      name: 'Transactions',
      component: Transactions,
    },
    {
      path: '/accounts',
      name: 'Accounts',
      component: Accounts,
    },
  ],

});
